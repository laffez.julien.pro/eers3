// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Wed Oct 11 12:37:48 2023
//
// Verilog Description of module GENDECAL
//

module GENDECAL (CLK, RST, SOUT);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(8[8:16])
    input CLK;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(10[3:6])
    input RST;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(11[3:6])
    output SOUT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(12[3:7])
    
    wire CLK_c /* synthesis is_clock=1, SET_AS_NETWORK=CLK_c */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(10[3:6])
    
    wire RST_c, SOUT_c_0;
    wire [15:0]QINT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(16[9:13])
    wire [15:0]QINTSUIVANT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(17[9:20])
    
    wire CLK_c_enable_8, VCC_net, GND_net;
    
    LUT4 i3_4_lut (.A(SOUT_c_0), .B(QINT[6]), .C(QINT[2]), .D(QINT[1]), 
         .Z(QINTSUIVANT[15])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C (D)+!C !(D)))+!A !(B (C (D)+!C !(D))+!B !(C (D)+!C !(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(30[14:57])
    defparam i3_4_lut.init = 16'h6996;
    FD1P3AY QINT__i2 (.D(QINT[2]), .SP(CLK_c_enable_8), .CK(CLK_c), .Q(QINT[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i2.GSR = "ENABLED";
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    OB SOUT_pad (.I(SOUT_c_0), .O(SOUT));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(12[3:7])
    LUT4 i26_1_lut (.A(RST_c), .Z(CLK_c_enable_8)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(11[3:6])
    defparam i26_1_lut.init = 16'h5555;
    IB CLK_pad (.I(CLK), .O(CLK_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(10[3:6])
    IB RST_pad (.I(RST), .O(RST_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(11[3:6])
    VLO i35 (.Z(GND_net));
    FD1P3AY QINT__i1 (.D(QINT[1]), .SP(CLK_c_enable_8), .CK(CLK_c), .Q(SOUT_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i1.GSR = "ENABLED";
    GSR GSR_INST (.GSR(CLK_c_enable_8));
    TSALL TSALL_INST (.TSALL(GND_net));
    FD1S3AX QINT__i3 (.D(QINT[3]), .CK(CLK_c), .Q(QINT[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i3.GSR = "ENABLED";
    FD1S3AX QINT__i4 (.D(QINT[4]), .CK(CLK_c), .Q(QINT[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i4.GSR = "ENABLED";
    FD1S3AX QINT__i5 (.D(QINT[5]), .CK(CLK_c), .Q(QINT[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i5.GSR = "ENABLED";
    FD1P3AY QINT__i6 (.D(QINT[6]), .SP(CLK_c_enable_8), .CK(CLK_c), .Q(QINT[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i6.GSR = "ENABLED";
    FD1P3AY QINT__i7 (.D(QINT[7]), .SP(CLK_c_enable_8), .CK(CLK_c), .Q(QINT[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i7.GSR = "ENABLED";
    FD1S3AX QINT__i8 (.D(QINT[8]), .CK(CLK_c), .Q(QINT[7]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i8.GSR = "ENABLED";
    FD1P3AY QINT__i9 (.D(QINT[9]), .SP(CLK_c_enable_8), .CK(CLK_c), .Q(QINT[8]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i9.GSR = "ENABLED";
    FD1P3AY QINT__i10 (.D(QINT[10]), .SP(CLK_c_enable_8), .CK(CLK_c), 
            .Q(QINT[9]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i10.GSR = "ENABLED";
    FD1S3AX QINT__i11 (.D(QINT[11]), .CK(CLK_c), .Q(QINT[10]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i11.GSR = "ENABLED";
    FD1S3AX QINT__i12 (.D(QINT[12]), .CK(CLK_c), .Q(QINT[11]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i12.GSR = "ENABLED";
    FD1S3AX QINT__i13 (.D(QINT[13]), .CK(CLK_c), .Q(QINT[12]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i13.GSR = "ENABLED";
    FD1P3AY QINT__i14 (.D(QINT[14]), .SP(CLK_c_enable_8), .CK(CLK_c), 
            .Q(QINT[13]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i14.GSR = "ENABLED";
    FD1P3AY QINT__i15 (.D(QINT[15]), .SP(CLK_c_enable_8), .CK(CLK_c), 
            .Q(QINT[14]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i15.GSR = "ENABLED";
    FD1S3AX QINT__i16 (.D(QINTSUIVANT[15]), .CK(CLK_c), .Q(QINT[15]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i16.GSR = "ENABLED";
    VHI i39 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

