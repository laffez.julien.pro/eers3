// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Sat Oct 28 12:14:57 2023
//
// Verilog Description of module TOP
//

module TOP (I_STDBY, O_DATA, O_DATA_VECT, I_RST);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(8[8:11])
    input I_STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(10[2:9])
    output O_DATA;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(11[2:8])
    output [4:0]O_DATA_VECT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    input I_RST;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(13[2:7])
    
    wire INT_CLK1 /* synthesis is_clock=1, SET_AS_NETWORK=INT_CLK1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(17[8:16])
    wire CPT /* synthesis is_clock=1, SET_AS_NETWORK=CPT */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(22[8:11])
    
    wire GND_net, I_STDBY_c, O_DATA_c_0, VCC_net, I_RST_c;
    wire [0:0]CPT_N_2;
    
    wire CPT_enable_8;
    
    OB O_DATA_VECT_pad_4 (.I(GND_net), .O(O_DATA_VECT[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    IB I_RST_pad (.I(I_RST), .O(I_RST_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(13[2:7])
    FD1S3AX CPT_8 (.D(CPT_N_2[0]), .CK(INT_CLK1), .Q(CPT));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(42[3] 49[10])
    defparam CPT_8.GSR = "DISABLED";
    OB O_DATA_VECT_pad_3 (.I(GND_net), .O(O_DATA_VECT[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    OSCH U1 (.STDBY(I_STDBY_c), .OSC(INT_CLK1)) /* synthesis syn_instantiated=1 */ ;
    defparam U1.NOM_FREQ = "2.08";
    LUT4 i35_1_lut (.A(I_RST_c), .Z(CPT_enable_8)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(13[2:7])
    defparam i35_1_lut.init = 16'h5555;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    GENDECAL U2 (.O_DATA_c_0(O_DATA_c_0), .CPT(CPT), .CPT_enable_8(CPT_enable_8));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(38[7:15])
    OB O_DATA_pad (.I(O_DATA_c_0), .O(O_DATA));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(11[2:8])
    IB I_STDBY_pad (.I(I_STDBY), .O(I_STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(10[2:9])
    OB O_DATA_VECT_pad_0 (.I(GND_net), .O(O_DATA_VECT[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    OB O_DATA_VECT_pad_1 (.I(GND_net), .O(O_DATA_VECT[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    OB O_DATA_VECT_pad_2 (.I(GND_net), .O(O_DATA_VECT[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(12[2:13])
    VLO i1 (.Z(GND_net));
    LUT4 i25_1_lut (.A(CPT), .Z(CPT_N_2[0])) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(47[16:19])
    defparam i25_1_lut.init = 16'h5555;
    GSR GSR_INST (.GSR(CPT_enable_8));
    TSALL TSALL_INST (.TSALL(GND_net));
    VHI i46 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module GENDECAL
//

module GENDECAL (O_DATA_c_0, CPT, CPT_enable_8);
    output O_DATA_c_0;
    input CPT;
    input CPT_enable_8;
    
    wire CPT /* synthesis is_clock=1, SET_AS_NETWORK=CPT */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/top.vhd(22[8:11])
    wire [15:0]QINT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(16[9:13])
    wire [15:0]QINTSUIVANT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(17[9:20])
    
    LUT4 i3_4_lut (.A(O_DATA_c_0), .B(QINT[6]), .C(QINT[2]), .D(QINT[1]), 
         .Z(QINTSUIVANT[15])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C (D)+!C !(D)))+!A !(B (C (D)+!C !(D))+!B !(C (D)+!C !(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(30[14:57])
    defparam i3_4_lut.init = 16'h6996;
    FD1S3AX QINT__i3 (.D(QINT[3]), .CK(CPT), .Q(QINT[2])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i3.GSR = "ENABLED";
    FD1P3AY QINT__i2 (.D(QINT[2]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[1])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i2.GSR = "ENABLED";
    FD1S3AX QINT__i4 (.D(QINT[4]), .CK(CPT), .Q(QINT[3])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i4.GSR = "ENABLED";
    FD1S3AX QINT__i5 (.D(QINT[5]), .CK(CPT), .Q(QINT[4])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i5.GSR = "ENABLED";
    FD1P3AY QINT__i6 (.D(QINT[6]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[5])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i6.GSR = "ENABLED";
    FD1P3AY QINT__i7 (.D(QINT[7]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[6])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i7.GSR = "ENABLED";
    FD1S3AX QINT__i8 (.D(QINT[8]), .CK(CPT), .Q(QINT[7])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i8.GSR = "ENABLED";
    FD1P3AY QINT__i9 (.D(QINT[9]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[8])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i9.GSR = "ENABLED";
    FD1P3AY QINT__i10 (.D(QINT[10]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[9])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i10.GSR = "ENABLED";
    FD1S3AX QINT__i11 (.D(QINT[11]), .CK(CPT), .Q(QINT[10])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i11.GSR = "ENABLED";
    FD1S3AX QINT__i12 (.D(QINT[12]), .CK(CPT), .Q(QINT[11])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i12.GSR = "ENABLED";
    FD1S3AX QINT__i13 (.D(QINT[13]), .CK(CPT), .Q(QINT[12])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i13.GSR = "ENABLED";
    FD1P3AY QINT__i14 (.D(QINT[14]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[13])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i14.GSR = "ENABLED";
    FD1P3AY QINT__i15 (.D(QINT[15]), .SP(CPT_enable_8), .CK(CPT), .Q(QINT[14])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i15.GSR = "ENABLED";
    FD1S3AX QINT__i16 (.D(QINTSUIVANT[15]), .CK(CPT), .Q(QINT[15])) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i16.GSR = "ENABLED";
    FD1P3AY QINT__i1 (.D(QINT[1]), .SP(CPT_enable_8), .CK(CPT), .Q(O_DATA_c_0)) /* synthesis LSE_LINE_FILE_ID=21, LSE_LCOL=7, LSE_RCOL=15, LSE_LLINE=38, LSE_RLINE=38 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/temp eer/gen decalage/proto1.vhd(22[3] 27[10])
    defparam QINT__i1.GSR = "ENABLED";
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

