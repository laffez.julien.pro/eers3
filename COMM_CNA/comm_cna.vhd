-- BLOC_COMM_CNA, KILLIAN LOUISE, JULIEN LAFFEZ
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY BLOC_COMM_CNA IS
	PORT(
		CLK : IN STD_LOGIC;
		TRIGG : IN STD_LOGIC;
		E : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		A : OUT STD_LOGIC;
		B : OUT STD_LOGIC;
		WR : OUT STD_LOGIC;
		CS : OUT STD_LOGIC;
		LDAC : OUT STD_LOGIC;
		CLR : OUT STD_LOGIC;
		D : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END BLOC_COMM_CNA;

ARCHITECTURE BEHAV OF BLOC_COMM_CNA IS
signal CPT : INTEGER RANGE 0 TO 13;
begin
	INCREMENT_HORLOGE : PROCESS(TRIGG, CLK)
	begin
		IF(CLK'EVENT AND CLK='1' AND (TRIGG='1' OR CPT>0)) then
			IF(CPT=13) THEN
				CPT <= 0;
			ELSE CPT <= CPT + 1;
			END IF;
		END IF;
	END PROCESS INCREMENT_HORLOGE;
A <= '0' WHEN (CPT>1 AND CPT<6) ELSE '1';
B <= '1' WHEN (CPT>1 AND CPT<6) ELSE '0';
CS <= '0' WHEN (CPT>1 AND CPT<6) ELSE '1';
WR <= '0' WHEN (CPT>1 AND CPT<6) ELSE '1';
D <= E WHEN (CPT>2 AND CPT<7) ELSE "00000000";
LDAC <= '0' WHEN (CPT>9 AND CPT<=13) ELSE '1';
CLR <= '0' WHEN (CPT>9 AND CPT<=13) ELSE '1';
END BEHAV;