EESchema Schematic File Version 4
LIBS:carte_fille-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x20_Male J8
U 1 1 654F6C5A
P 4550 2400
F 0 "J8" H 4656 3478 50  0000 C CNN
F 1 "Conn_01x20_Male" H 4656 3387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 4550 2400 50  0001 C CNN
F 3 "~" H 4550 2400 50  0001 C CNN
	1    4550 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1500 5000 1500
Wire Wire Line
	4750 1600 5000 1600
Wire Wire Line
	4750 1700 5000 1700
Wire Wire Line
	4750 1800 5000 1800
Wire Wire Line
	4750 1900 5000 1900
Wire Wire Line
	4750 2000 5000 2000
Wire Wire Line
	4750 2100 5000 2100
Wire Wire Line
	4750 2200 5000 2200
Wire Wire Line
	4750 2300 5000 2300
Wire Wire Line
	4750 2400 5000 2400
Wire Wire Line
	4750 2500 5000 2500
Wire Wire Line
	4750 2600 5000 2600
Wire Wire Line
	4750 2700 5000 2700
Wire Wire Line
	4750 2800 5000 2800
Wire Wire Line
	4750 2900 5000 2900
Wire Wire Line
	4750 3000 5000 3000
Wire Wire Line
	4750 3100 5000 3100
Wire Wire Line
	4750 3200 5000 3200
Wire Wire Line
	4750 3300 5000 3300
Wire Wire Line
	4750 3400 5000 3400
$Comp
L Connector:Conn_01x20_Male J9
U 1 1 654F6C75
P 5700 2400
F 0 "J9" H 5806 3478 50  0000 C CNN
F 1 "Conn_01x20_Male" H 5806 3387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 5700 2400 50  0001 C CNN
F 3 "~" H 5700 2400 50  0001 C CNN
	1    5700 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1500 6150 1500
Wire Wire Line
	5900 1600 6150 1600
Wire Wire Line
	5900 1700 6150 1700
Wire Wire Line
	5900 1800 6150 1800
Wire Wire Line
	5900 1900 6150 1900
Wire Wire Line
	5900 2000 6150 2000
Wire Wire Line
	5900 2100 6150 2100
Wire Wire Line
	5900 2200 6150 2200
Wire Wire Line
	5900 2300 6150 2300
Wire Wire Line
	5900 2400 6150 2400
Wire Wire Line
	5900 2500 6150 2500
Wire Wire Line
	5900 2600 6150 2600
Wire Wire Line
	5900 2700 6150 2700
Wire Wire Line
	5900 2800 6150 2800
Wire Wire Line
	5900 2900 6150 2900
Wire Wire Line
	5900 3000 6150 3000
Wire Wire Line
	5900 3100 6150 3100
Wire Wire Line
	5900 3200 6150 3200
Wire Wire Line
	5900 3300 6150 3300
Wire Wire Line
	5900 3400 6150 3400
Text GLabel 6150 1500 2    50   BiDi ~ 0
103
Text GLabel 6150 1700 2    50   BiDi ~ 0
2
Text GLabel 6150 1800 2    50   BiDi ~ 0
4
Text GLabel 6150 1900 2    50   BiDi ~ 0
6
Text GLabel 6150 2000 2    50   BiDi ~ 0
10
$Comp
L power:GND #PWR019
U 1 1 65509A09
P 6150 2100
F 0 "#PWR019" H 6150 1850 50  0001 C CNN
F 1 "GND" V 6155 1972 50  0000 R CNN
F 2 "" H 6150 2100 50  0001 C CNN
F 3 "" H 6150 2100 50  0001 C CNN
	1    6150 2100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 65509A3C
P 5000 2100
F 0 "#PWR014" H 5000 1850 50  0001 C CNN
F 1 "GND" V 5005 1972 50  0000 R CNN
F 2 "" H 5000 2100 50  0001 C CNN
F 3 "" H 5000 2100 50  0001 C CNN
	1    5000 2100
	0    -1   -1   0   
$EndComp
Text GLabel 6150 2200 2    50   BiDi ~ 0
12
Text GLabel 6150 2300 2    50   BiDi ~ 0
14
Text GLabel 6150 2500 2    50   BiDi ~ 0
20
Text GLabel 6150 2600 2    50   BiDi ~ 0
22
Text GLabel 6150 2800 2    50   BiDi ~ 0
24
Text GLabel 6150 2900 2    50   BiDi ~ 0
26
Text GLabel 6150 3100 2    50   BiDi ~ 0
28
Text GLabel 6150 3300 2    50   BiDi ~ 0
33
Text GLabel 6150 3400 2    50   BiDi ~ 0
35
$Comp
L power:GND #PWR020
U 1 1 65520DA1
P 6150 2400
F 0 "#PWR020" H 6150 2150 50  0001 C CNN
F 1 "GND" V 6155 2272 50  0000 R CNN
F 2 "" H 6150 2400 50  0001 C CNN
F 3 "" H 6150 2400 50  0001 C CNN
	1    6150 2400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 655232B3
P 6150 2700
F 0 "#PWR021" H 6150 2450 50  0001 C CNN
F 1 "GND" V 6155 2572 50  0000 R CNN
F 2 "" H 6150 2700 50  0001 C CNN
F 3 "" H 6150 2700 50  0001 C CNN
	1    6150 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 655257C5
P 5000 2700
F 0 "#PWR016" H 5000 2450 50  0001 C CNN
F 1 "GND" V 5005 2572 50  0000 R CNN
F 2 "" H 5000 2700 50  0001 C CNN
F 3 "" H 5000 2700 50  0001 C CNN
	1    5000 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 65527CD7
P 5000 2400
F 0 "#PWR015" H 5000 2150 50  0001 C CNN
F 1 "GND" V 5005 2272 50  0000 R CNN
F 2 "" H 5000 2400 50  0001 C CNN
F 3 "" H 5000 2400 50  0001 C CNN
	1    5000 2400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 6552A1E9
P 6150 3000
F 0 "#PWR022" H 6150 2750 50  0001 C CNN
F 1 "GND" V 6155 2872 50  0000 R CNN
F 2 "" H 6150 3000 50  0001 C CNN
F 3 "" H 6150 3000 50  0001 C CNN
	1    6150 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR023
U 1 1 6552C6FB
P 6150 3200
F 0 "#PWR023" H 6150 2950 50  0001 C CNN
F 1 "GND" V 6155 3072 50  0000 R CNN
F 2 "" H 6150 3200 50  0001 C CNN
F 3 "" H 6150 3200 50  0001 C CNN
	1    6150 3200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 6552EC0D
P 5000 3200
F 0 "#PWR018" H 5000 2950 50  0001 C CNN
F 1 "GND" V 5005 3072 50  0000 R CNN
F 2 "" H 5000 3200 50  0001 C CNN
F 3 "" H 5000 3200 50  0001 C CNN
	1    5000 3200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 6553111F
P 5000 3000
F 0 "#PWR017" H 5000 2750 50  0001 C CNN
F 1 "GND" V 5005 2872 50  0000 R CNN
F 2 "" H 5000 3000 50  0001 C CNN
F 3 "" H 5000 3000 50  0001 C CNN
	1    5000 3000
	0    -1   -1   0   
$EndComp
Text GLabel 5000 1700 2    50   BiDi ~ 0
1
Text GLabel 5000 1800 2    50   BiDi ~ 0
3
Text GLabel 5000 1900 2    50   BiDi ~ 0
5
Text GLabel 5000 2000 2    50   BiDi ~ 0
9
Text GLabel 5000 2200 2    50   BiDi ~ 0
11
Text GLabel 5000 2300 2    50   BiDi ~ 0
13
Text GLabel 5000 2500 2    50   BiDi ~ 0
19
Text GLabel 5000 2600 2    50   BiDi ~ 0
21
Text GLabel 5000 2800 2    50   BiDi ~ 0
23
Text GLabel 5000 2900 2    50   BiDi ~ 0
25
Text GLabel 5000 3100 2    50   BiDi ~ 0
27
Text GLabel 5000 3300 2    50   BiDi ~ 0
32
Text GLabel 5000 3400 2    50   BiDi ~ 0
34
NoConn ~ 5000 1500
NoConn ~ 5000 1600
NoConn ~ 6150 1600
$Comp
L Connector:Conn_01x20_Male J3
U 1 1 654ECBC6
P 2800 2400
F 0 "J3" H 2906 3478 50  0000 C CNN
F 1 "Conn_01x20_Male" H 2906 3387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 2800 2400 50  0001 C CNN
F 3 "~" H 2800 2400 50  0001 C CNN
	1    2800 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1500 3250 1500
Wire Wire Line
	3000 1700 3250 1700
Wire Wire Line
	3000 1800 3250 1800
Wire Wire Line
	3000 1900 3250 1900
Wire Wire Line
	3000 2000 3250 2000
Wire Wire Line
	3000 2100 3250 2100
Wire Wire Line
	3000 2200 3250 2200
Wire Wire Line
	3000 2300 3250 2300
Wire Wire Line
	3000 2400 3250 2400
Wire Wire Line
	3000 2500 3250 2500
Wire Wire Line
	3000 2600 3250 2600
Wire Wire Line
	3000 2700 3250 2700
Wire Wire Line
	3000 2800 3250 2800
Wire Wire Line
	3000 2900 3250 2900
Wire Wire Line
	3000 3000 3250 3000
Wire Wire Line
	3000 3100 3250 3100
Wire Wire Line
	3000 3200 3250 3200
Wire Wire Line
	3000 3300 3250 3300
Wire Wire Line
	3000 3400 3250 3400
Text GLabel 3250 1500 2    50   BiDi ~ 0
108
Text GLabel 3250 1700 2    50   BiDi ~ 0
112
Text GLabel 3250 1900 2    50   BiDi ~ 0
114
Text GLabel 3250 2000 2    50   BiDi ~ 0
117
$Comp
L power:GND #PWR029
U 1 1 654ECBE6
P 3250 1800
F 0 "#PWR029" H 3250 1550 50  0001 C CNN
F 1 "GND" V 3255 1672 50  0000 R CNN
F 2 "" H 3250 1800 50  0001 C CNN
F 3 "" H 3250 1800 50  0001 C CNN
	1    3250 1800
	0    -1   -1   0   
$EndComp
Text GLabel 3250 2300 2    50   BiDi ~ 0
122
Text GLabel 3250 2500 2    50   BiDi ~ 0
128
Text GLabel 3250 2700 2    50   BiDi ~ 0
131
Text GLabel 3250 2800 2    50   BiDi ~ 0
133
Text GLabel 3250 3100 2    50   BiDi ~ 0
139
Text GLabel 3250 3200 2    50   BiDi ~ 0
141
Text GLabel 3250 3300 2    50   BiDi ~ 0
143
$Comp
L power:GND #PWR030
U 1 1 654ECBFB
P 3250 2200
F 0 "#PWR030" H 3250 1950 50  0001 C CNN
F 1 "GND" V 3255 2072 50  0000 R CNN
F 2 "" H 3250 2200 50  0001 C CNN
F 3 "" H 3250 2200 50  0001 C CNN
	1    3250 2200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR031
U 1 1 654ECC01
P 3250 2600
F 0 "#PWR031" H 3250 2350 50  0001 C CNN
F 1 "GND" V 3255 2472 50  0000 R CNN
F 2 "" H 3250 2600 50  0001 C CNN
F 3 "" H 3250 2600 50  0001 C CNN
	1    3250 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 654ECC13
P 3250 3000
F 0 "#PWR032" H 3250 2750 50  0001 C CNN
F 1 "GND" V 3255 2872 50  0000 R CNN
F 2 "" H 3250 3000 50  0001 C CNN
F 3 "" H 3250 3000 50  0001 C CNN
	1    3250 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR033
U 1 1 654ECC19
P 3250 3400
F 0 "#PWR033" H 3250 3150 50  0001 C CNN
F 1 "GND" V 3255 3272 50  0000 R CNN
F 2 "" H 3250 3400 50  0001 C CNN
F 3 "" H 3250 3400 50  0001 C CNN
	1    3250 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 1600 3250 1600
Text GLabel 3250 1600 2    50   BiDi ~ 0
110
Text GLabel 3250 2100 2    50   BiDi ~ 0
120
Text GLabel 3250 2400 2    50   BiDi ~ 0
126
Text GLabel 3250 2900 2    50   BiDi ~ 0
137
$Comp
L Connector:Conn_01x20_Male J2
U 1 1 655004DF
P 1650 2400
F 0 "J2" H 1756 3478 50  0000 C CNN
F 1 "Conn_01x20_Male" H 1756 3387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 1650 2400 50  0001 C CNN
F 3 "~" H 1650 2400 50  0001 C CNN
	1    1650 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1500 2100 1500
Wire Wire Line
	1850 1700 2100 1700
Wire Wire Line
	1850 1800 2100 1800
Wire Wire Line
	1850 1900 2100 1900
Wire Wire Line
	1850 2000 2100 2000
Wire Wire Line
	1850 2100 2100 2100
Wire Wire Line
	1850 2200 2100 2200
Wire Wire Line
	1850 2300 2100 2300
Wire Wire Line
	1850 2400 2100 2400
Wire Wire Line
	1850 2500 2100 2500
Wire Wire Line
	1850 2600 2100 2600
Wire Wire Line
	1850 2700 2100 2700
Wire Wire Line
	1850 2800 2100 2800
Wire Wire Line
	1850 2900 2100 2900
Wire Wire Line
	1850 3000 2100 3000
Wire Wire Line
	1850 3100 2100 3100
Wire Wire Line
	1850 3200 2100 3200
Wire Wire Line
	1850 3300 2100 3300
Wire Wire Line
	1850 3400 2100 3400
Text GLabel 2100 1700 2    50   BiDi ~ 0
111
Text GLabel 2100 1900 2    50   BiDi ~ 0
113
Text GLabel 2100 2000 2    50   BiDi ~ 0
115
$Comp
L power:GND #PWR024
U 1 1 655004FD
P 2100 1800
F 0 "#PWR024" H 2100 1550 50  0001 C CNN
F 1 "GND" V 2105 1672 50  0000 R CNN
F 2 "" H 2100 1800 50  0001 C CNN
F 3 "" H 2100 1800 50  0001 C CNN
	1    2100 1800
	0    -1   -1   0   
$EndComp
Text GLabel 2100 2300 2    50   BiDi ~ 0
121
Text GLabel 2100 2500 2    50   BiDi ~ 0
127
Text GLabel 2100 2700 2    50   BiDi ~ 0
130
Text GLabel 2100 2800 2    50   BiDi ~ 0
132
Text GLabel 2100 3100 2    50   BiDi ~ 0
138
Text GLabel 2100 3200 2    50   BiDi ~ 0
140
Text GLabel 2100 3300 2    50   BiDi ~ 0
142
$Comp
L power:GND #PWR025
U 1 1 6550050A
P 2100 2200
F 0 "#PWR025" H 2100 1950 50  0001 C CNN
F 1 "GND" V 2105 2072 50  0000 R CNN
F 2 "" H 2100 2200 50  0001 C CNN
F 3 "" H 2100 2200 50  0001 C CNN
	1    2100 2200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 65500510
P 2100 2600
F 0 "#PWR026" H 2100 2350 50  0001 C CNN
F 1 "GND" V 2105 2472 50  0000 R CNN
F 2 "" H 2100 2600 50  0001 C CNN
F 3 "" H 2100 2600 50  0001 C CNN
	1    2100 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 65500516
P 2100 3000
F 0 "#PWR027" H 2100 2750 50  0001 C CNN
F 1 "GND" V 2105 2872 50  0000 R CNN
F 2 "" H 2100 3000 50  0001 C CNN
F 3 "" H 2100 3000 50  0001 C CNN
	1    2100 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR028
U 1 1 6550051C
P 2100 3400
F 0 "#PWR028" H 2100 3150 50  0001 C CNN
F 1 "GND" V 2105 3272 50  0000 R CNN
F 2 "" H 2100 3400 50  0001 C CNN
F 3 "" H 2100 3400 50  0001 C CNN
	1    2100 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 1600 2100 1600
Text GLabel 2100 1600 2    50   BiDi ~ 0
109
Text GLabel 2100 2100 2    50   BiDi ~ 0
119
Text GLabel 2100 2400 2    50   BiDi ~ 0
125
Text GLabel 2100 2900 2    50   BiDi ~ 0
136
NoConn ~ 2100 1500
$EndSCHEMATC
