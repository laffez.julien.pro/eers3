EESchema Schematic File Version 4
LIBS:carte_fille-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Étage conversion modulateur carte HERMES"
Date "2023-11-08"
Rev "1.1"
Comp "IUT GEII"
Comment1 "LAFFEZ J. - LOUISE K. - ANDRIEU J."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1150 1500 1100 400 
U 654E77F4
F0 "power_block" 50
F1 "power_block.sch" 50
$EndSheet
$Sheet
S 1150 900  1100 400 
U 654E781B
F0 "IQ_DAC" 50
F1 "IQ_DAC.sch" 50
$EndSheet
$Sheet
S 1200 2600 1050 400 
U 654F529C
F0 "pinout" 50
F1 "pinout.sch" 50
$EndSheet
$EndSCHEMATC
